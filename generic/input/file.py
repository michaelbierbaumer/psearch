from .input import Input

class File(Input):

	def __init__(self, path):
		Input.__init__(self)
		self.path = path

	def id(self):
		return self.path + self.trigger.tick

	def get(self):
		with open(self.path, 'ro') as f:
			self.data = f.read()
			self.len = len(self.data)