from .common import Processor
import re

class Regex(Processor):
	''' takes a compiled regex or string as input
	returns processed string '''
	def __init__(self, regex):
		if isinstance(regex, str):
			self.regex = re.compile(regex)
		else: # Todo verify if instance of re.compile()
			self.regex = regex

	def process(self, text):
		return self.regex.match(text)