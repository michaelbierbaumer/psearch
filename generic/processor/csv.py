from generic.processor.common import Processor
import csv
import io

class CSV(Processor):

	def __init__(self, *args, **kwargs):
		assert len(args) > 0, 'pass config object'
		self.parser_config = args[0]
		args = tuple(args[1:]) # removes first arg element
		Processor.__init__(self)

	def process(self, data):
		# todo check if data is string
		return list(csv.DictReader( io.StringIO(data), **self.parser_config ))