from generic.trigger.common import Trigger
import types

def renameargs(fun, **argmap):
	argmapreverse = { v: k for k,v in argmap.items() }
	def f(*args, **kwargs):
		return fun(*args, **{ argmapreverse.get(k, k):v for k,v in kwargs.items() })
	return f

class Processor():

	def __init__(self, *args, **kwargs):
		if len(args) == 0 and len(kwargs) == 0: # no rename
			return
		oldargs = list(self.process.__code__.co_varnames[1:])
		for a in args:
			assert a not in kwargs, "kwarg '{}' overwrites positional argument".format(a)
			oldarg = oldargs.pop(0)
			kwargs[oldarg] = a
		self.fields = oldargs+list(kwargs.values())
		self.process = renameargs(self.process, **kwargs)


	def required_fields(self):
		try:
			return self.fields
		except AttributeError as e:
			return self.process.__code__.co_varnames[1:]

	def process(self):
		''' implement this method! '''
		raise NotImplemented()

	def _process(self, **kwargs):
		args = {}
		for name in self.required_fields():
			args[name] = kwargs[name]
		return self.process(**args)

class StringLength(Processor):
	''' Returns string length ''' 
	def process(self, data):
		return len(data)

class Each(Processor, Trigger):
	def __init__(self, param_name):
		Processor.__init__(self)
		Trigger.__init__(self)
		self.param_name = param_name

	def required_fields(self):
		return [self.param_name]

	def _process(self, **kwargs):
		return [self.process(i) for i in kwargs[self.param_name]]

	def process(self, data):
		return self.process_all(data)
