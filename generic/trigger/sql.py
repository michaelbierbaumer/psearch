from .common import Trigger
import psycopg2, psycopg2.extras # TODO

class Sql(Trigger):
    queries = []
    lastrunfilter = None
    dbconfig = None

    def __init__(self, host, port, user, password, db):
        Trigger.__init__(self)
        self.dbconfig = {'host': host, 'port': port, 'user': user, 'password': password, 'db':db}

    def add_query(self, *args, **kwargs):
        assert len(kwargs) == 1, 'ERR: field name required. Eg. data="SELECT ..."'
        for field, query in kwargs.items():
            self.queries.append({'field': field, 'query': query, 'subqueries': []})
        return self


    def add_subquery(self, *args, **kwargs):
        assert len(kwargs) > 0, 'field name required. Eg. data="SELECT ..."'
        assert len(self.queries) > 0, 'ERR: add query first.'
        for field,query in kwargs.items():
            self.queries[-1]['subqueries'].append({'field': field, 'query': query, 'subqueries': []})
        return self

    def add_subsubquery(self, *args, **kwargs):
        assert len(kwargs) > 0, 'field name required. Eg. data="SELECT ..."'
        assert len(self.queries) > 0, 'ERR: add query first.'
        assert len(self.queries[-1]['subqueries']) > 0, 'ERR: add subquery to query first.'
        for field,query in kwargs.items():
            self.queries[-1]['subqueries'][-1]['subqueries'].append({'field': field, 'query': query, 'subqueries': []}) # TODO make n layer!
        return self

    def add_lastrunfilter(self, lastrunfilter):
        self.lastrunfilter = lastrunfilter
        return self

    def connect(self):
        raise NotImplemented()

    def disconnect(self):
        raise NotImplemented()



class PostgreSql(Sql):

    conn = None

    def connect(self):
        self.conn = psycopg2.connect("dbname='{db}' user='{user}' host='{host}' password='{password}'".format(**self.dbconfig))


    def disconnect(self):
        self.conn.close()

    def execute_query(self, cursor, query):
        cursor.execute(query)
        return cursor.fetchall()

    def process_query(self, cursor, query, parent_result={}):
        data = self.execute_query(cursor, query['query'].format(**parent_result))
        if len(query['subqueries']) > 0:
            for res in data:
                for sq in query['subqueries']:
                    res[sq['field']] = self.process_query(cursor, sq, res)
        return data

    def wait(self):
        self.connect()
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        data = {}
        for query in self.queries:
            data[query['field']] = self.process_query(cur, query)

        self.disconnect()
        return data


