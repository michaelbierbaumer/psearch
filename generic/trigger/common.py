import sys

class Trigger():
	def __init__(self):
		self.processors = {}
		self.outputs = []

	def addprocessor(self, prop, processor):
		self.processors[prop] = processor
		return self

	def wait(self):
		''' Rule when to trigger '''
		raise NotImplemented()

	def do_process(self, context):
		for name, processor in self.processors.items():
			if name in context:
				continue
			if set(context.keys()) & set(processor.required_fields()) == set(processor.required_fields()): # True if not run yet
				context[name] = processor._process(**context)
				return True # continue
		return False # finished

	def process_all(self, context):
		while self.do_process(context):
			pass
		for output in self.outputs:
			output.output(context)
		return context

	def run(self):
		self.context = self.wait()
		self.process_all(self.context)
		

class File(Trigger):
	''' reads file line by line '''
	def __init__(self, path):
		Trigger.__init__(self)
		self.path = path

	def wait(self):
		with open(self.path) as f:
			return {'data': f.read()}

class Stdin(Trigger):
	''' reads stdin and exits'''
	def __init__(self):
		Trigger.__init__(self)

	def wait(self):
		return {'line': self.stream.readline()}
