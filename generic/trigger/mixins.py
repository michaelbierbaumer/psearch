import datetime
import time

class Periodic():
	def __init__(self, interval):
		self.interval = interval

	def runforever(self):
		assert isinstance(self.interval, int) or isinstance(self.interval, datetime.timedelta), 'Interval required. Use integer for seconds, or timedelta'
		if isinstance(self.interval, int):
			self.interval = datetime.timedelta(seconds=self.interval)
		while True:
			super(Periodic, self).run()
			print('periodic trigger sleeping {} seconds.'.format(self.interval.total_seconds()))
			time.sleep(self.interval.total_seconds())
