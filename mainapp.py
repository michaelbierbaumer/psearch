from generic.trigger.common import Stdin
from generic.processor.common import StringLength
from generic.output.stdout import StdOut

trigger = Stdin()
trigger.processors['len'] = StringLength(data='line')
trigger.outputs.append(StdOut())
trigger.run()
