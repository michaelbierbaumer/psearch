# demo app for csv parsing
import generic.trigger.common as common_trigger
import generic.trigger.mixins as mixins
import generic.trigger.sql as sql
from generic.output.stdout import StdOut
from generic.processor.common import *
from generic.processor.csv import CSV

class FileWatcher(mixins.Periodic, common_trigger.File):
	def __init__(self):
		common_trigger.File.__init__(self,'/home/one/Downloads/Parking Crew Mapping.csv')
		mixins.Periodic.__init__(self, 10)

# watcher = FileWatcher()
# #watcher.processors['length'] = StringLength(data='data')
# watcher.processors['csv'] = CSV({'delimiter':','}, data='data')
# watcher.processors['new_csv'] = Each('csv').addprocessor('name', StringLength('Rook Account')).addprocessor('vname', StringLength('Voodoo Username')) # TODO
# watcher.outputs.append(StdOut())
# watcher.run()

sqltrigger = sql.PostgreSql(host="localhost", port="5432", password="SurfingFuel2013", db="rookreport", user="postgres")\
	.add_lastrunfilter('WHERE lastchanged > {lastrun}')\
	.add_query(data='SELECT id, name from rookreport_campaign')\
	.add_subquery(notes='SELECT note from rookreport_campaignnote where campaign_id={id}')
sqltrigger.outputs.append(StdOut())
sqltrigger.run()
# cleanstring = Replace('null', '') + Replace(',', '->') + Trim() 

# dict_tr.processors['terms'] = 
# 	Sub('rows', ['id', 'title', 'type'])
# 	.addprocessor( 'uncleaned_title', Format( '{ger} - {en}', 'ger', 'en') )
# 	.addprocessor( 'title', cleanstring('uncleaned_title') )
# 	.addprocessor( 'type', Value('dictionary'))
# 	.addprocessor( 'id', Format('{type}-{position}', 'type', 'position')
# # terms = [ {combinedtitle: 'name - sometitle', name: '',  title: ''} ]
# dict_tr.outputs = [
# 	ElasticUpdate(database='locahost', port=9200, index='dict', inputlist='terms')
# 	]


# ########################################################################################################################
# dbconfig = {'host':'localhost', 'port': 5432, 'db': 'rookreport'}
# demand_tr = trigger.PeriodicPostgres(60, dbconfig, 
# 	''' SELECT r.request_id as id, r.description, r.request_type_id , r.company as company, r.LAST_UPDATE_DATE, 
# 			rhd.VISIBLE_PARAMETER2 as pf, rhd.VISIBLE_PARAMETER3 as area, rhd.VISIBLE_PARAMETER4 as designer, 
# 			rhd.VISIBLE_PARAMETER5 as ev, rhd.VISIBLE_PARAMETER6 as real, r.STATUS_ID as status_id,
# 			rd.VISIBLE_PARAMETER1 as iver, rhd.VISIBLE_PARAMETER10 as ver, u.FULL_NAME as username,
# 			us.FULL_NAME as t_author
# 			FROM  PPM.KCRT_REQUESTS r
# 			JOIN PPM.KCRT_REQ_HEADER_DETAILS rhd ON rhd.REQUEST_ID=r.REQUEST_ID AND rhd.BATCH_NUMBER=1
# 			JOIN PPM.KCRT_REQUEST_DETAILS rd ON rd.REQUEST_ID=r.REQUEST_ID AND rd.BATCH_NUMBER=1
# 			LEFT JOIN PPM.KNTA_USERS us ON rd.CREATED_BY = us.USER_ID
# 			LEFT JOIN PPM.KNTA_USERS u ON r.ASSIGNED_TO_USER_ID = u.USER_ID
# 			WHERE R.REQUEST_TYPE_ID IN(30320,30260)
# 			AND RHD.CREATION_DATE >= '01-JAN-10'
# 			AND RHD.LAST_UPDATE_DATE >= TO_DATE(${lastRunDate},'yy-mm-dd HH24:MI')
# 			-- AND r.request_id>30000 AND r.request_id<70000
# 			ORDER BY RHD.LAST_UPDATE_DATE ''')

# #demand_tr['oe-map'] = input.Postgres(dbconfig, ''' SELECT LOOKUP_CODE, MEANING FROM PPM.knta_lookups WHERE lookup_type = "CRT - Company" ''' )
# #demand_tr['status-map'] = input.Obj({'4':'cancelled' , '78':'rejected' , '30188':'test' , '30189':'develop' , '30190':'prioritize' , '30191':'specify', '30192':'design' , '30193':'assign' , '30194':'completed' , '30208':'analyze' , '30408':'approve' , '30428':'entered'})
# #demand_tr['type-map'] = input.Obj({'30260':'Error' , '30320':'Request'})

# demand_tr['tickets'] = Sub('data', ['id', 'realname', 'version', 'iver', 'type', 'status' ,'oe', 'pf', 'area'])
# 	.addprocessor('version', Regex( '\d{1,2}.\d{1}', 'vers'))
# 	.addprocessor('title', Format('#{id} {description}', 'id', 'description'))
# 	.addprocessor('type', MapLookup('type-map', 'request_type_id'))
# 	.addprocessor('status', MapLookup('type-map', 'status_id'))
# 	.addprocessor('oe', MapLookup('oe-map', 'company'))
# 	.addprocessor('author', SwapNames('t_author'))

# demand_tr.outputs = [
# 	ElasticReplace(database="localhost", port=9200, index='demand', inputlist='tickets')
# ]

# ########################################################################################################################

# objective_tr = trigger.Path(500, '/home/one/Downloads/') 
# objective_tr['xlsfiles'] = Filter( lambda x: x.filename.endswith('.xsl') ).inputs( 'files' )
# objective_tr['value'] = Sub('xlsfiles').addprocessor( 'version', Regex( '[0-9]*', 'filename' ) )